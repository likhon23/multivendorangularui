import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Test } from '../model/Test';
import { Food } from '../model/Food';
import { ArticleDetails } from '../model/ArticleDetails';
import { ArticleVariant } from '../model/ArticleVariant';
import { ArticleImageVariant } from '../model/ArticleImageVariant';
import { EArticleDetails } from '../model/EArticleDetails';
import { FormGroup,FormControl, Validators,FormBuilder,FormArray } from '@angular/forms';
import *as _ from 'lodash';
import { EArticleImage } from '../model/EArticleImage';
import { ContactPerson } from '../model/ContactPerson';
import { CourierMaster } from '../model/CourierMaster';
import { Agreement } from '../model/Agreement';
import { OrderDetails } from '../model/OrderDetails';
import { DiscouuntArticle } from '../model/DiscouuntArticle';
import { DiscountDetails } from '../model/DiscountDetails';
import { NotificationMsg } from '../model/NotificationMsg';
import { userDetails } from '../model/userDetails';
@Injectable({
  providedIn: 'root'
})

export class UserService {

  form:FormGroup = new FormGroup({
    art_Code:new FormControl(null),
   
    articleImage:new FormControl('',Validators.required),
    isDefault:new FormControl(false)
  })
  articleVariantForm:FormGroup = new FormGroup({
    articleNo:new FormControl('',Validators.required),
    size:new FormControl(null,Validators.required),
    color:new FormControl('',Validators.required),
    gender:new FormControl('',Validators.required),
    quantity:new FormControl(null,Validators.required),
    artV_Id:new FormControl(0),
  })
  imageForm:FormGroup = new FormGroup({
    imageName:new FormControl('',Validators.required),
    isMaster:new FormControl(false),
    img_Id:new FormControl(0)
  })
 
  formData:User;
  TestData:Test;
  FoodData:Food;
  ArticleDetailsData:ArticleDetails;
  ArticleVariantData:ArticleVariant;
  ArticleImageData:ArticleImageVariant;
  ArticleVariantList:ArticleVariant[];
  ArticleImageList:ArticleImageVariant[];
  ArticleEImageList:EArticleImage[];
  ContactPersonList:ContactPerson[];
  SelectedItems:DiscouuntArticle[];
  order:string;
  
  orderDetailsList:OrderDetails[];
  orderDetailsList1:OrderDetails[];
  readonly authUrl= "https://localhost:44375/api/OAuth";
  readonly baseUrl ="https://localhost:44375/api"
  readonly imgUrl = "https://localhost:44375/Likhon/"

  constructor(private http:HttpClient) { }

  postUser(formData:User){
    return this.http.post(this.authUrl+'/GetAuthentication',formData);
  }

  getUser(){
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.authUrl+'/GetUser');
  }
  postTest(TestData:Test){
    return this.http.post(this.authUrl+'/PostTest',TestData);
  }

  getCategoryList() {
    return this.http.get(this.baseUrl + '/ArticleSettings/GetCategorys').toPromise();
  }
 
  getBrandList() {
    return this.http.get(this.baseUrl + '/ArticleSettings/GetBrand').toPromise();
  }
  getVatList() {
    return this.http.get(this.baseUrl + '/ArticleSettings/GetVat').toPromise();
  }
  postAllArticleData(ArticleDetailsData:ArticleDetails){
    var body = {
      ArticleDetailsData,
      ArticleImageList: this.ArticleImageList,
      ArticleVariantList:this.ArticleVariantList
    };
    return this.http.post(this.baseUrl+'/ArticleSettings/GetAllArticleData',body);
  }

  EGetAllArticleDetails(){
    return this.http.get(this.baseUrl+'/ArticleGet/EGetAllArticleDetails');
  }
  populateForm(row){
    
    
     this.form.patchValue({art_Code:row.art_Code})
  }

  postEArticleImage(){
  
    return this.http.post(this.baseUrl+'/ArticleSettings/PostArticleImage',this.ArticleEImageList);
  }

  postCourierMaster(CourierDetails:CourierMaster){
    var body = {
      CourierDetails,
      ContactPersonList:this.ContactPersonList
    }
    return this.http.post(this.baseUrl+'/ArticleSettings/PostCourierMaster',body);
  }

  getAllDistrictMaster(){
    return this.http.get(this.baseUrl+'/ArticleGet/GetAllDistrictMaster').toPromise();
  }
  getAllDistrictAreaById(id:number){
    return this.http.get(this.baseUrl+'/ArticleGet/GetDistrictMasterById/'+id).toPromise();
  }

  getCourierDetailsList(){
    return this.http.get(this.baseUrl+'/ArticleGet/GetCourierDetailsList').toPromise();
  }

  postAgreement(AgreementData:Agreement){
    return this.http.post(this.baseUrl+'/ArticleSettings/PostAgreement',AgreementData);
  }

  getOrderHeaderDetails(){
    return this.http.get(this.baseUrl+'/Order/GetOrderHeaderDetails');
  }
  orderNo(orderNo:string){
    this.order = orderNo;
  }
  getOrderDetailsByOrderNo(){
   
    
    return this.http.get(this.baseUrl+'/Order/GetOrderDetailsByOrderNo/'+this.order)
  }

  orderProcess(orderNo:string){
    return this.http.post(this.baseUrl+'/Order/OrderProcess/'+orderNo,orderNo);
  }
  //id wise get subcategory
  getSubCategoryById(id:number){
    return this.http.get(this.baseUrl+'/ArticleSettings/GetSubCategorys/'+id);
  }
  getSubSubCategoryById(id:number){
    return this.http.get(this.baseUrl+'/ArticleSettings/GetSubSubCategorys/'+id);
  }
  getSubSubSubCategoryById(id:number){
    return this.http.get(this.baseUrl+'/ArticleSettings/GetSubSubSubCategorys/'+id);
  }
  //3-2-2021
  getCatDiscountArtcleById(id:number){
    return this.http.get(this.baseUrl+'/ArticleGet/GetCatDiscountArticle/'+id);
  }
  getSubCatDiscountArtcleById(id:number){
    return this.http.get(this.baseUrl+'/ArticleGet/GetSubCatDiscountArticle/'+id);
  }
  getSubSubCatDiscountArtcleById(id:number){
    return this.http.get(this.baseUrl+'/ArticleGet/GetSubSubCatDiscountArticle/'+id);
  }
  
  getSubSubSCatDiscountArtcleById(id:number){
    return this.http.get(this.baseUrl+'/ArticleGet/GetSubSubSCatDiscountArticle/'+id);
  }
  //Post Discount
  postDiscountData(DiscountDetails:DiscountDetails){
    var body = {
      DiscountArticle: this.SelectedItems,
      DiscountDetails
    };
    return this.http.post(this.baseUrl+'/ArticleSettings/PostDiscountData',body);
  }

  //3-7-2021
  getArticleDetailsEditList(){
    return this.http.get(this.baseUrl+'/ArticleSettings/EditForArticleSettings').toPromise();
  }

  getImageListForEditById(id:number){
    return this.http.get(this.baseUrl+'/ArticleSettings/GetImageListForEdit/'+id).toPromise();
  }
  getArticleVariantListForEditById(id:number){
    return this.http.get(this.baseUrl+'/ArticleSettings/GetArticleVariantListForEdit/'+id).toPromise();
  }

  postNotificationMsg(NotificationVM:NotificationMsg){
    return this.http.post(this.baseUrl+'/MobilePost/Notification',NotificationVM);
  }
  //3/13/2021
  PickerProcess(orderNo:string){
    return this.http.post(this.baseUrl+'/Order/PickerProcess/'+orderNo,orderNo);
  }

  PackerProcess(orderNo:string){
    return this.http.post(this.baseUrl+'/Order/PackerProcess/'+orderNo,orderNo);
  }

  DispatchProcess(orderNo:string){
    return this.http.post(this.baseUrl+'/Order/DispatchProcess/'+orderNo,orderNo);
  }
  //3-14-2021
  getPickerHeaderDetails(){
    return this.http.get(this.baseUrl+'/Order/GetPickerHeader');
  }

  getPackerHeaderDetails(){
    return this.http.get(this.baseUrl+'/Order/GetPackerHeader');
  }

  getDispatchHeaderDetails(){
    return this.http.get(this.baseUrl+'/Order/GetDisptchHeader');
  }

  getAllCourierList(){
    return this.http.get(this.baseUrl+'/ArticleGet/GetAllCourierList/');
  }

  getAllAgreementList(){
    return this.http.get(this.baseUrl+'/ArticleGet/GetAllCourierAgreementList/');
  }
  //3-16-2021
  removeArticleImage(id:number){
    return this.http.post(this.baseUrl+'/ArticleSettings/RemoveArtImage/'+id,id);
  }

  removeArticleVariant(id:number){
    return this.http.post(this.baseUrl+'/ArticleSettings/RemoveArtVariant/'+id,id);
  }
  //3-17-2021
  removeArticleHistory(id:number){
    return this.http.post(this.baseUrl+'/ArticleSettings/RemoveArtHistory/'+id,id);
  }
}
