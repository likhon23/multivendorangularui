import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserService } from 'src/app/Shared/user.service';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit {
  @Output() sidenavClose = new EventEmitter();
  userDetails:{};
  a:number;
  b:number;
  c:number;
  d:number;
  constructor(public services:UserService) { }

  ngOnInit() {
    this.services.getUser().subscribe(res=>{
      this.userDetails = res;  
      this.authorization(res);
    },
    err=>{
      console.log(err);
    })
   
  }

  public onSidenavClose = () => {
    this.sidenavClose.emit();
  }
  authorization(row){
   if(row.roles=="Admin"){
    this.a=2;
   }else if(row.roles=="Picker"){
   this.b=3;
   }else if(row.roles=="Packer"){
   this.c=4;
   }else if(row.roles=="Super Admin"){
   this.d=5;
   }
   
}
}
