import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MatDialog,MatDialogConfig } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import { OrderHeader } from 'src/app/model/OrderHeader';
import { UserService } from 'src/app/Shared/user.service';
import { DetailsForPickerAndPackerComponent } from '../details-for-picker-and-packer/details-for-picker-and-packer.component';

@Component({
  selector: 'app-picker-header',
  templateUrl: './picker-header.component.html',
  styleUrls: ['./picker-header.component.css']
})
export class PickerHeaderComponent implements OnInit {
  searchKey:string;
  constructor( private dialog:MatDialog,public service:UserService, private toastr:ToastrService) { }

  ngOnInit() {
    this.service.getPickerHeaderDetails().subscribe(res=>
      
      this.dataSource.data = res as OrderHeader[]
      )
  }
  displayedColumns: string[] = ['Order No', 'Order Date', 'Total Quantity', 'Total Amount',
  'Payment Method','Paid Amount','action1','action2'];
  dataSource = new MatTableDataSource<OrderHeader>();

  onClear(){
    this.searchKey = "";
    this.applyFilter();
  }
  applyFilter(){
    this.dataSource.filter  = this.searchKey.trim().toLowerCase();
  }
  orderDetails(row){
    var res = row.order_No;
    this.service.orderNo(res);
    console.log("Order"+" "+res);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = "75%";
    dialogConfig.maxHeight=500;
     
    this.dialog.open(DetailsForPickerAndPackerComponent,dialogConfig);
  }

  processOrder(row){
    var res = row.order_No
    this.service.PickerProcess(res).subscribe((res:any)=>{
      this.service.getPickerHeaderDetails().subscribe(res=>
      
        this.dataSource.data = res as OrderHeader[]
        )
      this.toastr.success('Order Process Succesfull!','Order');
     
      },
      err=>{
        if(err.status == 400){
        this.toastr.error('Something Error!','Failed');
        }else{
          console.log(err);
        }
      })
  }
}
