import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MatDialog,MatDialogConfig } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import { OrderHeader } from 'src/app/model/OrderHeader';
import { UserService } from 'src/app/Shared/user.service';
import { OrderDetailsComponent } from '../order-details/order-details.component';
@Component({
  selector: 'app-dispatch',
  templateUrl: './dispatch.component.html',
  styleUrls: ['./dispatch.component.css']
})
export class DispatchComponent implements OnInit {
  searchKey:string;
  constructor(private dialog:MatDialog,public service:UserService, private toastr:ToastrService) { }

  ngOnInit() {
    this.service.getDispatchHeaderDetails().subscribe(res=>
      
      this.dataSource.data = res as OrderHeader[]
      )
  }
  displayedColumns: string[] = ['Order No', 'Order Date', 'Total Quantity', 'Total Amount',
  'Payment Method','Paid Amount','action1','action2'];
  dataSource = new MatTableDataSource<OrderHeader>();
 

  onClear(){
    this.searchKey = "";
    this.applyFilter();
  }
  applyFilter(){
    this.dataSource.filter  = this.searchKey.trim().toLowerCase();
  }

  orderDetails(row){
    var res = row.order_No;
    this.service.orderNo(res);
    console.log("Order"+" "+res);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = "75%";
    dialogConfig.maxHeight=500;
     
    this.dialog.open(OrderDetailsComponent,dialogConfig);
  }

  processOrder(row){
    var res = row.order_No
    this.service.DispatchProcess(res).subscribe((res:any)=>{
      this.service.getDispatchHeaderDetails().subscribe(res=>
      
        this.dataSource.data = res as OrderHeader[]
        )
      this.toastr.success('Order Process Succesfull!','Order');
     
      },
      err=>{
        if(err.status == 400){
        this.toastr.error('Something Error!','Failed');
        }else{
          console.log(err);
        }
      })
  }
}
