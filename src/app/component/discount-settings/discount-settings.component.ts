import { Component, OnInit,ViewChild,Output,EventEmitter } from '@angular/core';
import { FormGroup,FormControl, Validators,FormBuilder,FormArray } from '@angular/forms';
import { UserService } from 'src/app/Shared/user.service';
import { ToastrService } from 'ngx-toastr';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Category } from 'src/app/model/Category';
import { Vat } from 'src/app/model/Vat';
import {MatTableDataSource} from '@angular/material/table';
import{MatPaginator} from '@angular/material/paginator';
import { OrderHeader } from 'src/app/model/OrderHeader';
import {SelectionModel} from '@angular/cdk/collections';
import { from } from 'rxjs';
import { SubSubCatgory } from 'src/app/model/SubSubCatgory';
import { SubSubSubCat } from 'src/app/model/SubSUbSubCat';
import { SubCategory } from 'src/app/model/SubCategory';
import { DiscouuntArticle } from 'src/app/model/DiscouuntArticle';
import { HttpClient,HttpEventType } from '@angular/common/http';
@Component({
  selector: 'app-discount-settings',
  templateUrl: './discount-settings.component.html',
  styleUrls: ['./discount-settings.component.css']
})
export class DiscountSettingsComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  show =false;
  showS =false;  
  showSS =false; 
  searchKey:string;
  articleForm:FormGroup;
  agreementForm:FormGroup;
  CategoryList:Category[];
  SubCategoryList:SubCategory[];
  SubSubCatList:SubSubCatgory[];
  SubSubSubCatList:SubSubSubCat[];
  CatDiscountList:DiscouuntArticle[];
  public message:string;
  public progress:number;
  @Output() onUploadFinished = new EventEmitter();
  VatList:Vat[];
  categoryDiscountSettingsForm:FormGroup;
  DiscountSettingsForm:FormGroup;
  startDate = new Date(1990, 0, 1);
  constructor(public service:UserService,
    private toastr:ToastrService,private http:HttpClient) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.service.getCategoryList().then(res=>this.CategoryList = res as Category[]);
    this.service.getVatList().then(res=>this.VatList=res as Vat[]);
    this.categoryDiscountSettingsForm = new FormGroup({
      CategoryC_Id:new FormControl(null,Validators.required),
      SubCategoryS_Id:new FormControl(null,Validators.required),
      SubSubCategoryS_Id:new FormControl(0),
      SubSubSubCategoryS_Id:new FormControl(0),
  
    })
    this.DiscountSettingsForm = new FormGroup({
      DiscountPercentage:new FormControl(null,Validators.required),
      FromDate:new FormControl(null,Validators.required),
      ToDate:new FormControl(null,Validators.required),
      Banner:new FormControl(null,Validators.required),
      BannerName:new FormControl(null,Validators.required),
      Title:new FormControl(null),
      NotificationMsg:new FormControl(null),
    })
  
  }
  displayedColumns: string[] = ['select', 'ArticleTitle', 'StandardPrice', 'BrandName'];
  dataSource = new MatTableDataSource<DiscouuntArticle>();
  
  selection = new SelectionModel<DiscouuntArticle>(true, []);
  onClear(){
    this.searchKey = "";
    this.applyFilter();
  }
  applyFilter(){
    this.dataSource.filter  = this.searchKey.trim().toLowerCase();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    this.service.SelectedItems = this.selection.selected;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }
  getSubCat(id){
    
    this.service.getSubCategoryById(id).subscribe((res:any)=>{
      this.show = true;
     this.SubCategoryList = res as SubCategory[]; 
     
    },
    err=>{
     if(err.status == 400){
       this.show = false;
     }
   }
    )
    this.service.getCatDiscountArtcleById(id).subscribe((res:any)=>{

     this.dataSource.data = res as DiscouuntArticle[]; 
     
    },
    err=>{
     if(err.status == 400){
       
     }
   }
    )
 
   }
   getSubSubCat(id){
     this.service.getSubSubCategoryById(id).subscribe((res:any)=>{
       this.showS = true;
      this.SubSubCatList = res as SubSubCatgory[]; 
     },
     err=>{
      if(err.status == 400){
        this.showS = false;
      }
    }
     )
     this.service.getSubCatDiscountArtcleById(id).subscribe((res:any)=>{
  
     this.dataSource.data = res as DiscouuntArticle[]; 
     
    },
    err=>{
     if(err.status == 400){
 
     }
   }
    )
   }
   getSubSubSubCat(id){
     this.service.getSubSubSubCategoryById(id).subscribe((res:any)=>{
       this.showSS = true;
      this.SubSubSubCatList = res as SubSubSubCat[]; 
     },
     err=>{
      if(err.status == 400){
        this.showSS = false;
      }
    }
     )
     this.service.getSubCatDiscountArtcleById(id).subscribe((res:any)=>{
  
      this.dataSource.data = res as DiscouuntArticle[]; 
      
     },
     err=>{
      if(err.status == 400){
  
      }
    }
     )
   }
   getSubSubSubSCat(id){
  
   
    this.service.getSubSubSCatDiscountArtcleById(id).subscribe((res:any)=>{
 
     this.dataSource.data = res as DiscouuntArticle[]; 
     
    },
    err=>{
     if(err.status == 400){
 
     }
   }
    )
  }
  onSubmit(){
    
    if(this.DiscountSettingsForm.valid){
      this.service.postDiscountData(this.DiscountSettingsForm.value).subscribe(
        (res:any)=>{
         this.DiscountSettingsForm.reset();
         this.categoryDiscountSettingsForm.reset();
         this.show = false;
         this.showS = false;
         this.showSS = false;
         this.dataSource.data = [];
         this.toastr.success('Saved Succesfully!','Save');
        
         },
         err=>{
           if(err.status == 400){
           this.toastr.error('Incorrect Username or Password','Authentication Failed');
           }else{
             console.log(err);
           }
         }
      )
    }
  }

  public uploadFile =(files)=>{
    if(files.length==0)
    return;
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file',fileToUpload,fileToUpload.name);
    this.http.post('https://localhost:44375/api/productupload/Upload',formData,{reportProgress:true,observe:'events'})
                 .subscribe(event=>{
                   if(event.type ==HttpEventType.UploadProgress){
                     this.progress = Math.round(100*event.loaded/event.total)
                   }
                   else if(event.type == HttpEventType.Response){
                     this.message = "Upload Success";
                     this.onUploadFinished.emit(event.body);
                   }
                 })
  }
}
