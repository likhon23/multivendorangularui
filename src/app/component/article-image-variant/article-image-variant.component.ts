import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import {HttpClient, HttpEventType} from '@angular/common/http';
import { FormGroup,FormControl, Validators,FormBuilder,FormArray } from '@angular/forms';
import { UserService } from 'src/app/Shared/user.service';
@Component({
  selector: 'app-article-image-variant',
  templateUrl: './article-image-variant.component.html',
  styleUrls: ['./article-image-variant.component.css']
})
export class ArticleImageVariantComponent implements OnInit {
  imageForm:FormGroup;
  public message:string;
  public progress:number;
  @Output() onUploadFinished = new EventEmitter();
  constructor(public service:UserService,private http:HttpClient) { }

  ngOnInit() {
    //this.service.ArticleImageList = [];
  }
  onSubmit(){
    if(this.service.imageForm.valid){
      if(this.service.imageForm.value.img_Id==null||this.service.imageForm.value.img_Id==0){
      var d = this.service.imageForm.value.imageName;
      var d2 = d.substring(12,150);
      var d1 = this.service.imageForm.value.isMaster;
      if(d1==null){
        d1=false;
      }
      let obj = {
        imageName:d2,
        isMaster:d1,
        img_Id:0
      }
      this.service.ArticleImageList.push(obj);
    }else{
      var indx = this.service.ArticleImageList.findIndex(obj=>obj.img_Id==this.service.imageForm.value.img_Id);
      var d5 = this.service.imageForm.value.imageName;
      var d3 = d5.substring(12,150);
      var d4 = this.service.imageForm.value.isMaster;
      if(d4==null){
        d4=false;
      }
      let obj = {
        imageName:d3,
        isMaster:d4,
        img_Id:this.service.imageForm.value.img_Id
      }
      this.service.ArticleImageList[indx]=obj;
    }
      this.service.imageForm.reset();
    }
    
  }
  public uploadFile =(files)=>{
    if(files.length==0)
    return;
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file',fileToUpload,fileToUpload.name);
    this.http.post('https://localhost:44375/api/productupload/Upload',formData,{reportProgress:true,observe:'events'})
                 .subscribe(event=>{
                   if(event.type ==HttpEventType.UploadProgress){
                     this.progress = Math.round(100*event.loaded/event.total)
                   }
                   else if(event.type == HttpEventType.Response){
                     this.message = "Upload Success";
                     this.onUploadFinished.emit(event.body);
                   }
                 })
  }
}
