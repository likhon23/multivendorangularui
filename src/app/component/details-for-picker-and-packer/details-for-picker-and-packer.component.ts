import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/Shared/user.service';
import {MatTableDataSource} from '@angular/material/table';
import { OrderDetails } from 'src/app/model/OrderDetails';

@Component({
  selector: 'app-details-for-picker-and-packer',
  templateUrl: './details-for-picker-and-packer.component.html',
  styleUrls: ['./details-for-picker-and-packer.component.css']
})
export class DetailsForPickerAndPackerComponent implements OnInit {

  constructor(public service:UserService) { }
  OrderDetails = [];
  ngOnInit() {
    this.service.getOrderDetailsByOrderNo().subscribe(res=>{
     
      this.service.orderDetailsList= res as OrderDetails[],
      this.dataSource.data = res as OrderDetails[]
    }
      )
  }
  displayedColumns: string[] = ['Article', 'Article No',
  'Size'];

 
  dataSource = new MatTableDataSource<OrderDetails>();
}
