import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsForPickerAndPackerComponent } from './details-for-picker-and-packer.component';

describe('DetailsForPickerAndPackerComponent', () => {
  let component: DetailsForPickerAndPackerComponent;
  let fixture: ComponentFixture<DetailsForPickerAndPackerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsForPickerAndPackerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsForPickerAndPackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
