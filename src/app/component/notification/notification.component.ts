import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl, Validators,FormBuilder,FormArray } from '@angular/forms';
import { UserService } from 'src/app/Shared/user.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient,HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  DiscountSettingsForm:FormGroup;
  constructor(public service:UserService,
    private toastr:ToastrService,private http:HttpClient) { }

  ngOnInit() {
    this.DiscountSettingsForm = new FormGroup({
      Title:new FormControl(null),
      NotificationMsg:new FormControl(null),
    })
  }
  onSubmit(){
    
    if(this.DiscountSettingsForm.valid){
      this.service.postNotificationMsg(this.DiscountSettingsForm.value).subscribe(
        (res:any)=>{
         this.DiscountSettingsForm.reset();
  
         this.toastr.success('Notification Sent  Succesfully!','Sent');
        
         },
         err=>{
           if(err.status == 400){
           this.toastr.error('Something Error To Sent Notification','Failed');
           }else{
             console.log(err);
           }
         }
      )
    }
  }
}
