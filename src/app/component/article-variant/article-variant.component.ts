import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl, Validators,FormBuilder,FormArray } from '@angular/forms';
import { UserService } from 'src/app/Shared/user.service';
@Component({
  selector: 'app-article-variant',
  templateUrl: './article-variant.component.html',
  styleUrls: ['./article-variant.component.css']
})
export class ArticleVariantComponent implements OnInit {
  articleVariantForm:FormGroup;
  constructor(public service:UserService) { }

  ngOnInit() {
  
    //this.service.ArticleVariantList = [];
  }
  onSubmit(){
    if(this.service.articleVariantForm.valid){
      var indx = this.service.ArticleVariantList.findIndex(obj=>obj.articleNo==this.service.articleVariantForm.value.articleNo);
      if(indx>-1){
        this.service.ArticleVariantList[indx] = this.service.articleVariantForm.value;
      }else{
        if(this.service.articleVariantForm.value.artV_Id==null){
          let obj={
            artV_Id:0,
            articleNo:this.service.articleVariantForm.value.articleNo,
            gender:this.service.articleVariantForm.value.gender,
            color:this.service.articleVariantForm.value.color,
            size:this.service.articleVariantForm.value.size,
            quantity:this.service.articleVariantForm.value.quantity
          }
          this.service.ArticleVariantList.push(obj);
          this.service.articleVariantForm.reset();
        }else{
          this.service.ArticleVariantList.push(this.service.articleVariantForm.value);
          this.service.articleVariantForm.reset();
        }
  
      }
 
    }
  }
}
