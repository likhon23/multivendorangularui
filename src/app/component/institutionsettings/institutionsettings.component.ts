import { Component, OnInit,ViewChild} from '@angular/core';
import { UserService } from 'src/app/Shared/user.service';
import { FormGroup,FormControl, Validators,FormBuilder,FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Test } from 'src/app/model/Test';
import { Category } from 'src/app/model/Category';
import { SubCategory } from 'src/app/model/SubCategory';
import { Brand } from 'src/app/model/Brand';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { MatDialog,MatDialogConfig } from '@angular/material/dialog';
import { ArticleImageVariantComponent } from '../article-image-variant/article-image-variant.component';
import { ArticleVariantComponent } from '../article-variant/article-variant.component';
import { Vat } from 'src/app/model/Vat';
import { ArticleVariant } from 'src/app/model/ArticleVariant';
import { ArticleImageVariant } from 'src/app/model/ArticleImageVariant';
import { SubSubCatgory } from 'src/app/model/SubSubCatgory';
import { SubSubSubCat } from 'src/app/model/SubSUbSubCat';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import { ArticleDetailsEdit } from 'src/app/model/ArticleDetailsEdit';
import {MatSort} from '@angular/material/sort';
@Component({
  selector: 'app-institutionsettings',
  templateUrl: './institutionsettings.component.html',
  styleUrls: ['./institutionsettings.component.css']
})
export class InstitutionsettingsComponent implements OnInit {
  CategoryList:Category[];
  SubCategoryList:SubCategory[];
  SubSubCatList:SubSubCatgory[];
  SubSubSubCatList:SubSubSubCat[];
  BrandList:Brand[];
  VatList:Vat[];
  ShowVat:Vat[];
  show =false;
  showS =false;  
  showSS =false; 
  articleForm:FormGroup;
  ImageList:ArticleImageVariant[];
  fg:FormArray;
  searchKey:string;
  ArticleDetailsEditList:ArticleDetailsEdit[];
  constructor(public service:UserService,
    private toastr:ToastrService,
    private _formBuilder: FormBuilder,
    private dialog:MatDialog) { }
    @ViewChild(MatSort, {static: true}) sort: MatSort;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
   this.service.getCategoryList().then(res=>this.CategoryList = res as Category[]);
   //this.service.getSubCategoryList().then(res=>this.SubCategoryList=res as SubCategory[]);
   this.service.getBrandList().then(res=>this.BrandList=res as Brand[]);
   this.service.getVatList().then(res=>this.VatList=res as Vat[]);
   this.service.getArticleDetailsEditList().then(res=> this.dataSource.data=res as ArticleDetailsEdit[]);
   this.dataSource.paginator = this.paginator;
  this.articleForm = new FormGroup({
    ArticleTitle:new FormControl('',Validators.required),
    ArticleSubTitle:new FormControl('',Validators.required),
    CategoryC_Id:new FormControl(null,Validators.required),
    SubCategoryS_Id:new FormControl(null,Validators.required),
    SubSubCategoryS_Id:new FormControl(0),
    SubSubSubCategoryS_Id:new FormControl(0),
    Brand_Id:new FormControl(null,Validators.required),
    Vat_Id:new FormControl(null,Validators.required),
    Description:new FormControl('',Validators.required),
    StandardPrice:new FormControl(null,Validators.required),
    FranchisePrice:new FormControl(null,Validators.required),
    InstitutePrice:new FormControl(null,Validators.required),
    PurchaseCost:new FormControl(null,Validators.required),
    WholeSalePrice:new FormControl(null,Validators.required),
    DealerPrice:new FormControl(null,Validators.required),
    OtherPrice:new FormControl(null,Validators.required),
    IsNewAraival:new FormControl(false),
    ArticleId: new FormControl(0),
    PricingId:new FormControl(0),
    ArticleCode:new FormControl(null,Validators.required)
  
  })
  this.service.ArticleImageList = [];
  this.service.ArticleVariantList = [];
  this.ShowVat = this.VatList; 
  }
  
    
 
 

  config: AngularEditorConfig = { editable: true, spellcheck: true, height: '3rem', minHeight: '3rem', placeholder: 'Enter Article Description  here...', translate: 'no' }

  getSubCat(id){
    
   this.service.getSubCategoryById(id).subscribe((res:any)=>{
     this.show = true;
    this.SubCategoryList = res as SubCategory[]; 
   },
   err=>{
    if(err.status == 400){
      this.show = false;
    }
  }
   )
  }
  getSubSubCat(id){
    this.service.getSubSubCategoryById(id).subscribe((res:any)=>{
      this.showS = true;
     this.SubSubCatList = res as SubSubCatgory[]; 
    },
    err=>{
     if(err.status == 400){
       this.showS = false;
     }
   }
    )
  }
  getSubSubSubCat(id){
    this.service.getSubSubSubCategoryById(id).subscribe((res:any)=>{
      this.showSS = true;
     this.SubSubSubCatList = res as SubSubSubCat[]; 
    },
    err=>{
     if(err.status == 400){
       this.showSS = false;
     }
   }
    )
  }
  onSubmit(){
    
    if(this.articleForm.valid){
      this.service.postAllArticleData(this.articleForm.value).subscribe(
        (res:any)=>{
         this.articleForm.reset();
         this.service.ArticleImageList.length=0;
         this.service.ArticleVariantList.length=0;
         this.toastr.success('Saved Succesfully!','Save');
         this.show = false;
         this.showS = false;
         this.showSS = false;
         window.location.reload(); 
         },
         err=>{
           if(err.status == 400){
           this.toastr.error('Incorrect Username or Password','Authentication Failed');
           }else{
             console.log(err);
             
           }
         }
      )
    }
  }

  AddOrEditAriticleVariant(){
   
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = "50%";
     
    this.dialog.open(ArticleVariantComponent,dialogConfig);
  }
  AddOrEditImage(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = "35%";
     
    this.dialog.open(ArticleImageVariantComponent,dialogConfig);
  }

  displayedColumns: string[] = ['Article Name', 'Article Code', 'Price', 'Action1','Action2'];
  dataSource = new MatTableDataSource<ArticleDetailsEdit>();
  onClear(){
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter(){
    this.dataSource.filter = this.searchKey.trim().toLowerCase();
  }

  EditArticle(row){
  this.articleForm.setValue({
    ArticleId:row.artD_Id,
    PricingId:row.pricing_Id,
    ArticleTitle:row.articleTitle,
    ArticleSubTitle:row.articleSubTitle,
    CategoryC_Id:row.categoryC_Id,
    SubCategoryS_Id:row.subCategoryS_Id,
    SubSubCategoryS_Id:row.subSubCategoryS_Id,
    SubSubSubCategoryS_Id:row.subSubSubCategoryS_Id,
    Brand_Id:row.brand_Id,
    Vat_Id:row.vat_Id,
    Description:row.description,
    StandardPrice:row.standardPrice,
    FranchisePrice:row.franchisePrice,
    InstitutePrice:row.institutePrice,
    PurchaseCost:row.purchaseCost,
    WholeSalePrice:row.wholeSalePrice,
    DealerPrice:row.dealerPrice,
    OtherPrice:row.otherPrice,
    IsNewAraival:row.isNewAraival,
    ArticleCode:row.articleCode
  });
  this.service.getImageListForEditById(row.artD_Id).then(res=>  this.service.ArticleImageList = res as ArticleImageVariant[]);
  this.service.getArticleVariantListForEditById(row.artD_Id).then(res=>  this.service.ArticleVariantList = res as ArticleVariant[]);
  }

  DeleteArticle(row){
    this.service.removeArticleHistory(row.artD_Id).subscribe(
      (res:any)=>{
    
       this.toastr.success('Deleted Succesfully!','Remove');
       this.service.getArticleDetailsEditList().then(res=> this.dataSource.data=res as ArticleDetailsEdit[]);
      
       },
       err=>{
         if(err.status == 400){
         this.toastr.error('Something Error found!','Deleted Failed');
         }else{
           console.log(err);
           
         }
       }
    )
  }

  EditArticleVariant(item){
    this.service.articleVariantForm.setValue({
      articleNo:item.articleNo,
      size:item.size,
      color:item.color,
      gender:item.gender,
      quantity:item.quantity,
      artV_Id:item.artV_Id
    })
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = "50%";
     
    this.dialog.open(ArticleVariantComponent,dialogConfig);
   
  }
  DeleteArticleVariant(item){
    if(item.artV_Id==0){
      var indx = this.service.ArticleVariantList.findIndex(obj=>obj.articleNo==item.articleNo);
      this.service.ArticleVariantList.splice(indx,1);
    }else{
      this.service.removeArticleVariant(item.artV_Id).subscribe(
        (res:any)=>{
          var indx = this.service.ArticleVariantList.findIndex(obj=>obj.articleNo==item.articleNo);
          this.service.ArticleVariantList.splice(indx,1);
         this.toastr.success('Deleted Succesfully!','Remove');
        
        
         },
         err=>{
           if(err.status == 400){
           this.toastr.error('Something Error found!','Deleted Failed');
           }else{
             console.log(err);
             
           }
         }
      )
    }
  
  
  }
  EditArticleImage(item){
    this.service.imageForm.patchValue({
    
      isMaster:item.isMaster,
      img_Id:item.img_Id
    })
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = "35%";
     
    this.dialog.open(ArticleImageVariantComponent,dialogConfig);
  }
  DeleteArticleImage(item){
    if(item.img_Id==0){
      var indx = this.service.ArticleImageList.findIndex(obj=>obj.img_Id==item.img_Id);
      this.service.ArticleImageList.splice(indx,1);
    }else{
      this.service.removeArticleImage(item.img_Id).subscribe(
        (res:any)=>{
      
         this.toastr.success('Deleted Succesfully!','Save');
        
        
         },
         err=>{
           if(err.status == 400){
           this.toastr.error('Something Error found!','Deleted Failed');
           }else{
             console.log(err);
             
           }
         }
      )
    }
    
  }
}
