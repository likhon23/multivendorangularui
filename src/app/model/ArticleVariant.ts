export class ArticleVariant{
     artV_Id:number;
     articleNo:string;
     gender:string; 
     color:string;
     size:number; 
  
     quantity:number;
}