export class Agreement {
    RegionId: number;
    CityId: number;
    CompanyId:number;
    DeliveryAmount:number;
    Remarks:string;
}