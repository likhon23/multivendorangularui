export class DiscountDetails{
    DiscountPercentage:number;
    FromDate:Date;
    ToDate:Date;
    Banner:string;
    BannerName:string;
    Title:string;
    NotificationMsg:string;
   }