type GUID = string & { isGuid: true};

export class OrderHeader {
     Order_No:string;
     CustomerName:string;
     CustomerMobileNo:string;
     OrderHeaderId:GUID;

    Order_Date:Date;

    Total_Quantity:number;
    OrderAmount:number;

    Payment_Through:string;
    Total_payable_Amount:number;
    Paid_Amount:number;
}