import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { HomeComponent } from 'src/app/component/home/home.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/Shared/shared.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { SettinggsComponent } from 'src/app/component/settinggs/settinggs.component';
import{FormsModule,ReactiveFormsModule} from '@angular/forms';
import { InstitutionsettingsComponent } from 'src/app/component/institutionsettings/institutionsettings.component';
import { UsersettingsComponent } from 'src/app/component/usersettings/usersettings.component';
import {MatExpansionModule} from '@angular/material/expansion'; 
import{MatButtonModule} from '@angular/material/button';
import{MatFormFieldModule} from '@angular/material/form-field';
import{MatInputModule} from '@angular/material/input';
import{MatRippleModule} from '@angular/material/core';
import {MatGridListModule} from '@angular/material/grid-list'; 
import { ToastrModule } from 'ngx-toastr';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatDialogModule} from '@angular/material/dialog';
import { ArticleImageVariantComponent } from 'src/app/component/article-image-variant/article-image-variant.component';
import { ArticleVariantComponent } from 'src/app/component/article-variant/article-variant.component';
import{QuillModule} from 'ngx-quill'
import { HttpClientModule} from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ArticleSettingsComponent } from 'src/app/component/article-settings/article-settings.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import{MatIconModule} from '@angular/material/icon';
//start
import{MatAutocompleteModule} from '@angular/material/autocomplete';
import{MatButtonToggleModule} from '@angular/material/button-toggle';
import{MatCardModule} from '@angular/material/card';
import{MatChipsModule} from '@angular/material/chips';
import{MatDatepickerModule} from '@angular/material/datepicker';
import{MatListModule} from '@angular/material/list';
import{MatMenuModule} from '@angular/material/menu';
import{MatNativeDateModule} from '@angular/material/core';
import{MatProgressBarModule} from '@angular/material/progress-bar';
import{MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import{MatRadioModule} from '@angular/material/radio';
import{MatSliderModule} from '@angular/material/slider';
import{MatSlideToggleModule} from '@angular/material/slide-toggle';
import{MatSnackBarModule} from '@angular/material/snack-bar';
import{MatTabsModule} from '@angular/material/tabs';
import{MatToolbarModule} from '@angular/material/toolbar';
import{MatTooltipModule} from '@angular/material/tooltip';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

//end
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSortModule} from '@angular/material/sort';
import { AddArticleImageComponent } from 'src/app/component/add-article-image/add-article-image.component';
import { CouriermasterComponent } from 'src/app/component/couriermaster/couriermaster.component';
import { CouriercontactpersonComponent } from 'src/app/component/couriercontactperson/couriercontactperson.component';
import { AgrementSettingsComponent } from 'src/app/component/agrement-settings/agrement-settings.component';
import { OrderHeaderComponent } from 'src/app/component/order-header/order-header.component';
import { OrderDetailsComponent } from 'src/app/component/order-details/order-details.component';
import { PickerHeaderComponent } from 'src/app/component/picker-header/picker-header.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {A11yModule} from '@angular/cdk/a11y';
import {BidiModule} from '@angular/cdk/bidi';
import {ObserversModule} from '@angular/cdk/observers';
import {OverlayModule} from '@angular/cdk/overlay';
import {PlatformModule} from '@angular/cdk/platform';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';

import { DiscountSettingsComponent } from 'src/app/component/discount-settings/discount-settings.component';
import { NotificationComponent } from 'src/app/component/notification/notification.component';
import { PackerComponent } from 'src/app/component/packer/packer.component';
import { DispatchComponent } from 'src/app/component/dispatch/dispatch.component';
import { DetailsForPickerAndPackerComponent } from 'src/app/component/details-for-picker-and-packer/details-for-picker-and-packer.component';
import { from } from 'rxjs';


@NgModule({
  declarations: [
    MainComponent,
   HomeComponent,
   SettinggsComponent,
   InstitutionsettingsComponent,
   UsersettingsComponent,
   ArticleImageVariantComponent,
   ArticleVariantComponent,
   ArticleSettingsComponent,
   AddArticleImageComponent,
   CouriermasterComponent,
   CouriercontactpersonComponent,
   AgrementSettingsComponent,
   OrderHeaderComponent,
   OrderDetailsComponent,
   PickerHeaderComponent,
  DiscountSettingsComponent,
  NotificationComponent,
  PackerComponent,
  DispatchComponent,
  DetailsForPickerAndPackerComponent,
  
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,
    FormsModule,
    MatExpansionModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    ReactiveFormsModule,
    MatGridListModule, 
    MatSelectModule,
    MatTableModule,
    MatDialogModule,
    HttpClientModule,
    AngularEditorModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatIconModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatDatepickerModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSortModule,
    MatCheckboxModule,
    A11yModule,
    BidiModule,
    ObserversModule,
    OverlayModule,
    PlatformModule,
    PortalModule,
    ScrollingModule,
    CdkStepperModule,
    CdkTableModule,
    FlexLayoutModule,
    QuillModule.forRoot(),
    ToastrModule.forRoot(),
    FontAwesomeModule
  ],
  entryComponents:[ArticleImageVariantComponent,ArticleVariantComponent,
    AddArticleImageComponent,CouriercontactpersonComponent,
    OrderDetailsComponent,DetailsForPickerAndPackerComponent],
  

})
export class MainModule { }
